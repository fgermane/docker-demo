# Simple Docker application to test AWS ECS and EKS

## Build the Docker image and deploy it to the Docker registry

[build and publish docker image](docker.md)

## [Option 1] Run on EKS

- Prerequisite : install command line interface for AWS EKS: [eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)

### [Option 1.1] EKS with EC2 cluster

- Create an EKS cluster:
  ```
  eksctl create cluster --name my-eks-cluster --region eu-west-3
  ```

- Set up an [Application Load Balancer](app-load-balancer.md)


### [Option 1.2] EKS with Fargate (serverless)

- Create an EKS cluster:
  ```
  eksctl create cluster -f kube-config/cluster-fargate.yaml
  ```

  Sources :
  
  https://aws.amazon.com/fr/blogs/containers/using-alb-ingress-controller-with-amazon-eks-on-fargate/

  https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller

  ```

Add following flags to at least two subnets : 
kubernetes.io/cluster/'my-eks-cluster-fargate2': 'shared'
kubernetes.io/role/elb: 1

- Set up an [Application Load Balancer](app-load-balancer.md)

### Deploy the application on the cluster

- Create a name-space for the demo application:
  ```
  kubectl create namespace eks-hello-app
  ```

- Deploy the application:
  ```
  kubectl apply -f kube-config/deployment.yaml
  kubectl apply -f kube-config/service.yaml
  kubectl apply -f kube-config/ingress.yaml
  ```

- To get the external FQDN of the application:

  ```
  kubectl get ingress -n eks-hello-app

  NAME                CLASS    HOSTS   ADDRESS                                                                   PORTS   AGE
  eks-hello-ingress   <none>   *       k8s-ekshello-ekshello-391942ba43-1358873546.eu-west-3.elb.amazonaws.com   80      2m3s
  ```

- Troubleshooting:

  Chech ingress status is OK:

  ```
  kubectl describe ingress eks-hello-ingress -n eks-hello-app
  ```
  
  Rem: Typically, ingress path in yaml file should not be "/*" but "/".

- Delete the cluster:
  ```
  eksctl delete cluster --name my-eks-cluster --region eu-west-3
  ```

## Utils

- Connect to a pod:

  ```
  kubectl exec -n eks-hello-app -it pod-name-here -- bin/bash
  ```

## [Option 2] Run on ECS

### [Option 2.1] ECS with EC2 cluster

### [Option 2.2] ECS with Fargate (serverless)


## Extra

### Configure a Cloud9 environment

[config-cloud9](./config-cloud9.md)

### Implement GitOps with ArgoCD 

[ArgoCD](./ArgoCD.md)

### Tagging AWS resources

[tagging](./tagging.md)