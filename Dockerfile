FROM openjdk:14
COPY target/hello-docker-0.0.1-SNAPSHOT.jar hello-docker.jar
ENTRYPOINT ["java","-jar","/hello-docker.jar"]