# Installation and usage if Argo CD to automate deployement of the Kubernetes application

## Installation

Source: [Installation guidelines](https://argo-cd.readthedocs.io/en/stable/getting_started/)

Install Argo CD on the Kubernetes cluster:
```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

```

Expose Argo CD API to Internet, through a load balancer, in order to enable access the Argi CD UI and CLI:
```
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

Retreive default ArgoCD admin password: 
```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
```

Connect to the ArgoCD server through the CLI:
- First, get the server IP address:
```
kubectl get services -n argocd
...
NAME                    TYPE           CLUSTER-IP       EXTERNAL-IP
argocd-server           LoadBalancer   10.100.176.183   a3b75302a4c94466abf8b3e065a2c955-2076920807.eu-west-3.elb.amazonaws.com   80:31336/TCP,443:30569/TCP   16m
```
    Get the EXTERNAL-IP value.

- Then test log in with ArgoCD CLI:
```
argocd login a3b75302a4c94466abf8b3e065a2c955-2076920807.eu-west-3.elb.amazonaws.com
```

## Deploy the application

Connect to ArgoCD GUI :
[https://\<EXTERNAL-IP\>/applications?proj=&sync=&health=&namespace=&cluster=&labels=](https://a3b75302a4c94466abf8b3e065a2c955-2076920807.eu-west-3.elb.amazonaws.com/applications?proj=&sync=&health=&namespace=&cluster=&labels=)
