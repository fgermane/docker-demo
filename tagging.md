# Test of the tagging of AWS resources

- Pre-requisite :
  Tag all resources related to our hello application :
  - EC2 instances
  - EKS cluster
  ...

- Get list of resources for a given tag:

```
aws resourcegroupstaggingapi get-resources --tag-filters "Key=my-tag/appli,Values=hello"
```