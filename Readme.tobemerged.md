# Simple Docker application to test AWS ECS and EKS

## Build the Docker image and deploy it to the Docker registry

[build and publish docker image](docker.md)

## [Option 1] Run on EKS

- Prerequisite : install command line interface for AWS EKS: [eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)

### [Option 1.1] EKS with EC2 cluster

- Create an EKS cluster:
  ```
  eksctl create cluster --name my-eks-cluster --region eu-west-3
  ```

- Set up an [Application Load Balancer](app-load-balancer.md)


### [Option 1.2] EKS with Fargate (serverless)

- Create an EKS cluster:
  ```
  eksctl create cluster -f kube-config/cluster-fargate.yaml
  ```

  Sources :
  
  https://aws.amazon.com/fr/blogs/containers/using-alb-ingress-controller-with-amazon-eks-on-fargate/

  https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller

  ```

Add following flags to at least two subnets : 
kubernetes.io/cluster/'my-eks-cluster-fargate2': 'shared'
kubernetes.io/role/elb: 1

- Set up an [Application Load Balancer](app-load-balancer.md)

### Deploy the application on the cluster

- Create a name-space for the demo application:
  ```
  kubectl create namespace eks-hello-app
  ```

- Deploy the application:
  ```
  kubectl apply -f kube-config/deployment.yaml
  kubectl apply -f kube-config/service.yaml
  kubectl apply -f kube-config/ingress.yaml
  ```

- To get the external FQDN of the application:

  ```
  kubectl get ingress -n eks-hello-app

  NAME                CLASS    HOSTS   ADDRESS                                                                   PORTS   AGE
  eks-hello-ingress   <none>   *       k8s-ekshello-ekshello-391942ba43-1358873546.eu-west-3.elb.amazonaws.com   80      2m3s
  ```

- Troubleshooting:

  Chech ingress status is OK:

  ```
  kubectl describe ingress eks-hello-ingress -n eks-hello-app
  ```
  
  Rem: Typically, ingress path in yaml file should not be "/*" but "/".

- Delete the cluster:
  ```
  eksctl delete cluster --name my-eks-cluster --region eu-west-3
  ```

<<<<<<< HEAD

************************
Enable logging
 --> cf guidelines: https://docs.aws.amazon.com/eks/latest/userguide/fargate-logging.html

 kubectl apply -f kube-config/aws-observability.yaml 
 kubectl apply -f kubectl apply -f kube-config/aws-logging-cloudwatch-configmap.yaml 
 curl -o permissions.json https://raw.githubusercontent.com/aws-samples/amazon-eks-fluent-logging-examples/mainline/examples/fargate/cloudwatchlogs/permissions.json
 aws iam create-policy --policy-name eks-fargate-logging-policy --policy-document file://permissions.json
 aws iam attach-role-policy --policy-arn arn:aws:iam::${AWS_ACCOUNT_ID}:policy/eks-fargate-logging-policy --role-name eksctl-my-eks-cluster-farg-FargatePodExecutionRole-1A6UU7QVK2TZ8

***********************

eksctl utils associate-iam-oidc-provider  --region eu-west-3  --cluster my-eks-cluster-fargate2  --approve

curl -o iam_policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.2.0/docs/install/iam_policy.json

aws iam create-policy --policy-name AWSLoadBalancerControllerIAMPolicy --policy-document file://iam_policy.json

rm iam_policy.json

eksctl create iamserviceaccount --cluster my-eks-cluster-fargate2 --namespace kube-system --name aws-load-balancer-controller --attach-policy-arn arn:aws:iam::${ACCOUNT_ID}:policy/AWSLoadBalancerControllerIAMPolicy --override-existing-serviceaccounts --approve

kubectl get sa aws-load-balancer-controller -n kube-system -o yaml

kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master"



Set LBC_VERSION:

```
echo 'export LBC_VERSION="v2.3.0"' >>  ~/.bash_profile
.  ~/.bash_profile

```

Deploy the Helm chart from the Amazon EKS charts repo:

helm repo add eks https://aws.github.io/eks-charts

export VPC_ID=$(aws eks describe-cluster --name my-eks-cluster-fargate2 --query "cluster.resourcesVpcConfig.vpcId" --output text)

helm upgrade -i aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=my-eks-cluster-fargate2 --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller --set image.tag="${LBC_VERSION}" --set region=eu-west-3 --set vpcId=${VPC_ID} --set image.repository=kishorj/aws-load-balancer-controller

kubectl -n kube-system rollout status deployment aws-load-balancer-controller


- Create a name-space for the demo application and deploy it
=======
## Utils
>>>>>>> 35e4fb4b484c55b806394bdde805f306e557678f

- Connect to a pod:

  ```
  kubectl exec -n eks-hello-app -it pod-name-here -- bin/bash
  ```

## [Option 2] Run on ECS

### [Option 2.1] ECS with EC2 cluster

### [Option 2.2] ECS with Fargate (serverless)


## Extra

### Configure a Cloud9 environment

[config-cloud9](./config-cloud9.md)

### Implement GitOps with ArgoCD 

[ArgoCD](./ArgoCD.md)

### Tagging AWS resources

[tagging](./tagging.md)