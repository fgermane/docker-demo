# Configuration of AWS Cloud9 environment

- In Cloud9 IDE, in menu Preferences>AWS Settings, deactivate 'AWS Managed Credentials'

- Create a new Access Key to enable the Cloud9 IDE to connect to AWS services :

    - In AWS console > IAM > Access Management > Users :

    - Select my user, then select Security Credentials, then press "Create Access Key"

    - Apply the Access Key in the Cloud9 IDE :
    
        ```
        aws configure

        AWS Access Key ID [None]: XXXX
        AWS Secret Access Key [None]: yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
        Default region name [eu-west-3]: 
        Default output format [json]: 
        ```

- Configure Kubeclt: 

    - Copy the ./kube/config file from the original machine where the EKS ckuster has been created to the the Cloud9 IDE.
    
        NB: This may not be a very clean approach, but not found any way to make a working kube config with the command line...

- Test if it works:

    ```
    kubectl get svc

    NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
    kubernetes   ClusterIP   XXX   <none>        443/TCP   7d21h
    ```

- Add new cluster:

   ```
   aws eks update-kubeconfig --name new-cluster-name
   ```
